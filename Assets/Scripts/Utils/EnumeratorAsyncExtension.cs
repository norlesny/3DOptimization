using System.Collections;
using System.Runtime.CompilerServices;

namespace Utils
{
	public static class EnumeratorAsyncExtension
	{
		public static TaskAwaiter GetAwaiter(this IEnumerator enumerator)
		{
			return CoroutinePool.RunAsync(enumerator).GetAwaiter();
		}
	}
}
