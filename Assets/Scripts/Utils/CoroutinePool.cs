using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Utils
{
	public class CoroutinePool
	{
		private static CoroutinePoolBehaviour pool;

		private static CoroutinePoolBehaviour Pool
		{
			get
			{
				if (pool == null)
				{
					pool = new GameObject(nameof(CoroutinePool)).AddComponent<CoroutinePoolBehaviour>();
				}

				return pool;
			}
		}

		public static Task RunAsync(IEnumerator coroutine)
		{
			var taskCompletionSource = new TaskCompletionSource<bool>();
			Pool.StartCoroutine(RunCoroutineAsync(coroutine, taskCompletionSource));

			return taskCompletionSource.Task;
		}

		public static void RunDelayed(UnityAction action)
		{
			Pool.StartCoroutine(delayedAction(action));
		}
		
		private static IEnumerator RunCoroutineAsync(IEnumerator coroutine, TaskCompletionSource<bool> taskCompletionSource)
		{
			yield return coroutine;
			taskCompletionSource.SetResult(true);
		}

		private static IEnumerator delayedAction(UnityAction action)
		{
			yield return new WaitForEndOfFrame();
			action.Invoke();
		}

		private class CoroutinePoolBehaviour : MonoBehaviour
		{
			// Nothing
		}
	}
}
