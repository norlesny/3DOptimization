using UnityEngine;

namespace Logger
{
	public interface ILogger
	{
		void Log(string message);
		void Log(string message, GameObject gameObject);
	}
}
