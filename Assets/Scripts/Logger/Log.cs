namespace Logger
{
	public static class Log
	{
		private static readonly ITimedLogger logger = new TimedLogger(new DebugLogger());
		public static ITimedLogger Logger => logger;
	}
}
