using UnityEngine;

namespace Logger
{
	public class DebugLogger : ILogger
	{
		public void Log(string message)
		{
			Log(message, null);
		}

		public void Log(string message, GameObject gameObject)
		{
			Debug.Log(message, gameObject);
		}
	}
}
