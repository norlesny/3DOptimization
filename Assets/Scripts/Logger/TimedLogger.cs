using System.Diagnostics;
using UnityEngine;

namespace Logger
{
	public class TimedLogger : ITimedLogger
	{
		private readonly ILogger logger;
		private Stopwatch stopwatch;

		public TimedLogger(ILogger logger)
		{
			this.logger = logger;
		}

		public void Log(string message)
		{
			Log(message, null);
		}

		public void Log(string message, GameObject gameObject)
		{
			if (stopwatch == null)
			{
				StartTimer();
			}
			
			logger.Log($"{stopwatch.Elapsed.TotalSeconds:F2}s: {message}", gameObject);
		}

		public void StartTimer()
		{
			if (stopwatch == null)
			{
				stopwatch = Stopwatch.StartNew();
			}
			else
			{
				stopwatch.Start();
			}
		}

		public void StopTimer()
		{
			stopwatch?.Stop();
		}
	}
}
