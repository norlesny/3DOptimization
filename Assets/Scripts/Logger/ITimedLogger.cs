namespace Logger
{
	public interface ITimedLogger : ILogger
	{
		void StartTimer();
		void StopTimer();
	}
}
