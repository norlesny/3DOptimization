using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Logger;
using UnityEngine;
using Utils;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace Optimization
{
	public static class Optimization
	{
		public static async void OptimizeViewAsync(OptimizationSettings settings)
		{
			Log.Logger.Log("Starting optimization");
			await SplitSubmeshes();
			await AddMeshColliders();
			await ReplaceMeshes(settings);
			await DestroyColliders();
			Log.Logger.Log("Optimization finished");
		}

		public static IEnumerator SplitSubmeshes()
		{
			foreach (MeshFilter meshFilter in Object.FindObjectsOfType<MeshFilter>())
			{
				Mesh mesh = meshFilter.mesh;
				int subMeshCount = mesh.subMeshCount;
				if (subMeshCount <= 1)
				{
					continue;
				}

				var materials = meshFilter.GetComponent<MeshRenderer>().sharedMaterials;
				var prototype = meshFilter.gameObject;
				var numberOfSubmeshesToUse = Math.Min(subMeshCount, materials.Length);
				for (int i = 1; i < numberOfSubmeshesToUse; i++)
				{
					GameObject submeshObject = Object.Instantiate(prototype, prototype.transform.parent, true);
					var submeshMeshFilter = submeshObject.GetComponent<MeshFilter>();
					var newMesh = CopyMesh(mesh);
					newMesh.triangles = mesh.GetTriangles(i);
					submeshMeshFilter.mesh = newMesh;
					submeshObject.GetComponent<MeshRenderer>().materials = new[] {materials[i]};
				}

				mesh.triangles = mesh.GetTriangles(0);
				meshFilter.GetComponent<MeshRenderer>().materials = new[] {materials[0]};
			}

			Log.Logger.Log("Finished SplitSubmeshes");
			yield return null;
		}

		private static IEnumerator AddMeshColliders()
		{
			foreach (Collider collider in Object.FindObjectsOfType<Collider>())
			{
				Object.Destroy(collider);
			}
			foreach (MeshFilter meshFilter in Object.FindObjectsOfType<MeshFilter>())
			{
				GameObject gameObject = meshFilter.gameObject;
				var meshCollider = gameObject.AddComponent<MeshCollider>();
				meshCollider.sharedMesh = meshFilter.sharedMesh;
			}

			Log.Logger.Log("Finished AddMeshColliders");
			yield return null;
		}

		private static IEnumerator GetHitTriangles(OptimizationSettings settings, IDictionary<MeshCollider, HashSet<int>> result)
		{
			float horizontalStep = 1f / settings.horizontalRaycastDensity;
			float verticalStep = 1f / settings.verticalRaycastDensity;
			int camerasNumber = settings.CamerasNumber;
			int cameraCount = 0;
			Vector3 point = Vector3.zero;
			var hits = new List<(MeshCollider, int)>();
			foreach (Camera camera in settings.Cameras)
			{
				float x = 0f;
				while (x <= 1f)
				{
					float y = 0f;
					while (y <= 1f)
					{
						point.x = x;
						point.y = y;
						hits.Add(RaycastThroughPoint(point, result, camera));

						y += verticalStep;
					}

					x += horizontalStep;
				}

				Log.Logger.Log($"Calculating HitTriangles - {(int) (++cameraCount * 100f/camerasNumber)}%");
				yield return null;
			}

			foreach (IGrouping<MeshCollider, (MeshCollider, int)> group in hits.GroupBy(tuple => tuple.Item1))
			{
				if (group.Key == null)
				{
					continue;
				}

				HashSet<int> triangleIndexes;
				if (!result.TryGetValue(group.Key, out triangleIndexes))
				{
					triangleIndexes = new HashSet<int>();
					result.Add(group.Key, triangleIndexes);
				}

				foreach ((MeshCollider, int) tuple in group)
				{
					triangleIndexes.Add(tuple.Item2);
				}
			}

			yield return null;
		}

		private static (MeshCollider, int) RaycastThroughPoint(Vector3 point, IDictionary<MeshCollider, HashSet<int>> hitTriangles,
			Camera camera)
		{
			Ray ray = camera.ViewportPointToRay(point);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				var meshCollider = hit.collider as MeshCollider;
				if (meshCollider == null || meshCollider.sharedMesh == null)
				{
					Log.Logger.Log($"Not MeshCollider - {hit.collider.name}", hit.collider.gameObject);
					return (null, 0);
				}

				return (meshCollider, hit.triangleIndex);
			}
			
			return (null, 0);
		}

		private static Mesh CreateNewMesh(MeshCollider collider, HashSet<int> usedTriangles)
		{
				var sharedMesh = collider.sharedMesh;
				var sharedMeshVertices = sharedMesh.vertices;
				int[] sharedMeshTriangles = sharedMesh.triangles;
				Vector3[] sharedMeshNormals = sharedMesh.normals;
				Vector2[] sharedMeshUv = sharedMesh.uv;
				var sharedMeshUvLength = sharedMeshUv.Length;
				Vector2[] sharedMeshUv2 = sharedMesh.uv2;
				var sharedMeshUv2Length = sharedMeshUv2.Length;
				Vector4[] sharedMeshTangents = sharedMesh.tangents;

				//Log.Logger.Log("sharedMeshVertices:" + sharedMeshVertices.ToStringExt());
				//Log.Logger.Log("sharedMeshTriangles:" + sharedMeshTriangles.ToStringExt());
				//Log.Logger.Log("usedTriangles:" + usedTriangles.ToStringExt());

				var usedVertices = new List<int>();
				foreach (int triangleIndex in usedTriangles)
				{
					usedVertices.Add(sharedMeshTriangles[triangleIndex * 3 + 0]);
					usedVertices.Add(sharedMeshTriangles[triangleIndex * 3 + 1]);
					usedVertices.Add(sharedMeshTriangles[triangleIndex * 3 + 2]);
				}
				//Log.Logger.Log("usedVertices:" + usedVertices.ToStringExt());

				var usedVerticesSorted = new HashSet<int>(usedVertices).ToList();
				usedVerticesSorted.Sort();
				//Log.Logger.Log("usedVerticesSorted:" + usedVerticesSorted.ToStringExt());


				var newVertices = new List<Vector3>();
				var newNormals = new List<Vector3>();
				var newUv = new List<Vector2>();
				var newUv2 = new List<Vector2>();
				var newTangents = new List<Vector4>();
				var verticesMapping = new Dictionary<int, int>();
				for (int i = 0; i < usedVerticesSorted.Count; i++)
				{
					int index = usedVerticesSorted[i];
					newVertices.Add(sharedMeshVertices[index]);
					newNormals.Add(sharedMeshNormals[index]);
					if (sharedMeshUvLength > index)
					{
						newUv.Add(sharedMeshUv[index]);
					}
					if (sharedMeshUv2Length > index)
					{
						newUv2.Add(sharedMeshUv2[index]);
					}
					newTangents.Add(sharedMeshTangents[index]);
					verticesMapping.Add(index, i);
				}
				//Log.Logger.Log("newVertices:" + newVertices.ToStringExt());

				var newTriangles = new List<int>();
				foreach (int triangleIndex in usedTriangles)
				{
					newTriangles.Add(verticesMapping[sharedMeshTriangles[triangleIndex * 3 + 0]]);
					newTriangles.Add(verticesMapping[sharedMeshTriangles[triangleIndex * 3 + 1]]);
					newTriangles.Add(verticesMapping[sharedMeshTriangles[triangleIndex * 3 + 2]]);
				}


				Mesh mesh = CopyMesh(collider.sharedMesh);
				mesh.triangles = newTriangles.ToArray();
				mesh.vertices = newVertices.ToArray();
				mesh.normals = newNormals.ToArray();
				mesh.uv = newUv.ToArray();
				mesh.uv2 = newUv2.ToArray();
				mesh.tangents = newTangents.ToArray();
				return mesh;
		}

		private static IEnumerator ReplaceMeshes(OptimizationSettings settings)
		{
			Log.Logger.Log("Starting GetHitTriangles");
			yield return null;
			var hitTriangles = new Dictionary<MeshCollider, HashSet<int>>();
			yield return GetHitTriangles(settings, hitTriangles);
			if (!hitTriangles.Values.Any())
			{
				Log.Logger.Log("No triangles hit");
				yield break;
			}
			
			Log.Logger.Log("Finished GetHitTriangles");
			Log.Logger.Log("Starting ReplaceMeshes");
			yield return null;

			List<MeshCollider> allMeshColliders = Object.FindObjectsOfType<MeshCollider>().ToList();
			Log.Logger.Log($"Vertex count before: {allMeshColliders.Sum(collider => collider.sharedMesh.vertexCount)}");
			Log.Logger.Log(
				$"Triangles count before: {allMeshColliders.Sum(collider => collider.sharedMesh.triangles.Length / 3)}");
			yield return null;

			foreach (KeyValuePair<MeshCollider, HashSet<int>> kvp in hitTriangles)
			{
				MeshCollider meshCollider = kvp.Key;
				Mesh mesh = CreateNewMesh(meshCollider, kvp.Value);
				meshCollider.sharedMesh = mesh;

				var meshFilter = meshCollider.transform.GetComponent<MeshFilter>();
				if (meshFilter != null)
				{
					meshFilter.sharedMesh = mesh;
				}

				allMeshColliders.Remove(meshCollider);
			}

			Log.Logger.Log($"MeshColliders that were not hit: {allMeshColliders.Count}");
			foreach (MeshCollider meshCollider in allMeshColliders)
			{
				meshCollider.gameObject.SetActive(false);
			}

			Log.Logger.Log("Finished ReplaceMeshes");
			yield return null;
		}

		private static IEnumerator DestroyColliders()
		{
			List<MeshCollider> meshColliders = Object.FindObjectsOfType<MeshCollider>().ToList();
			Log.Logger.Log($"Vertex count after: {meshColliders.Sum(collider => collider.sharedMesh.vertexCount)}");
			Log.Logger.Log(
				$"Triangles count after: {meshColliders.Sum(collider => collider.sharedMesh.triangles.Length / 3)}");
			meshColliders.ForEach(Object.Destroy);

			yield return null;
		}

		private static Mesh CopyMesh(Mesh toCopy)
		{
			return new Mesh
			{
				vertices = toCopy.vertices,
				triangles = toCopy.triangles,
				uv = toCopy.uv,
				normals = toCopy.normals,
				colors = toCopy.colors,
				tangents = toCopy.tangents
			};
		}
	}
}
