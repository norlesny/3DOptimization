﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

public class Merger : MonoBehaviour
{
	[SerializeField] private GameObject[] objects;

	private async void Start()
	{
		await Optimization.Optimization.SplitSubmeshes();
		
		var meshFilter = transform.GetComponent<MeshFilter>();
		MeshFilter[] meshFilters = FindObjectsOfType<MeshFilter>().Where(filter => filter != meshFilter).ToArray();
		IEnumerable<IGrouping<Material, MeshFilter>> meshFiltersByMaterial =
			meshFilters.GroupBy(filter => filter.gameObject.GetComponent<MeshRenderer>().sharedMaterial);

		var meshes = new List<(MeshFilter, MeshRenderer)>();
		foreach (IGrouping<Material, MeshFilter> grouping in meshFiltersByMaterial)
		{
			meshes.Add(MergeMeshes(grouping, grouping.Key));
		}

		MergeMeshes(meshes);
	}

	private (MeshFilter, MeshRenderer) MergeMeshes(IEnumerable<MeshFilter> filters, Material material)
	{
		var combine = new List<CombineInstance>();
		foreach (MeshFilter filter in filters)
		{
			combine.Add(new CombineInstance
			{
				mesh = filter.sharedMesh,
				transform = filter.transform.localToWorldMatrix
			});
			filter.gameObject.SetActive(false);
		}
		
		var o = new GameObject();
		o.transform.parent = transform;
		
		var mesh = new Mesh();
		mesh.CombineMeshes(combine.ToArray());
		var meshFilter = o.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
		var meshRenderer = o.AddComponent<MeshRenderer>();
		meshRenderer.material = material;

		return (meshFilter, meshRenderer);
	}

	private void MergeMeshes(IEnumerable<(MeshFilter, MeshRenderer)> elements)
	{
		var combine = new List<CombineInstance>();
		var materials = new List<Material>();
		foreach ((MeshFilter, MeshRenderer) element in elements)
		{
			combine.Add(new CombineInstance
			{
				mesh = element.Item1.sharedMesh,
				transform = element.Item1.transform.localToWorldMatrix
			});
			materials.Add(element.Item2.sharedMaterial);
			element.Item1.gameObject.SetActive(false);
		}
		
		var o = new GameObject();
		o.transform.parent = transform;
		
		var mesh = new Mesh();
		mesh.CombineMeshes(combine.ToArray(), false);
		o.AddComponent<MeshFilter>().mesh = mesh;
		o.AddComponent<MeshRenderer>().materials = materials.ToArray();
		Debug.Log($"Number of final meshes: {elements.Count()}");
	}
}
